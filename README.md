# linux\_downloader

Automatically download the latest version of some of the most common linux
distributions. It will automatically download either the iso or the torrent file
for all supported linux distributions. If it already exists it will skip over
it, if it is partially downloaded it will continue the download. If the download
is an iso, it will validate the file with a checksum, if it fails it will remove
the downloaded file. It is recommened to download a torrent file and output it
to a watch folder for a torrent client to not abuse the servers.

# Usage

```bash
$ ./linux_downloader.sh [options]
```

# Options

```bash
General Options:
    -h, --help            Display this help message
    -o, --output          Change the download directory, Default is current directory
    --aria, --aria2       Download using aria2 instead of curl
    --parallel            Amount of downloads in parallel via curl or aria2, default is 1
    --iso                 Download iso files
    --torrent             Download torrent files
    --archlinux           Download Archlinux,             will not download others unless selected
    --debian              Download Debian,                will not download others unless selected
    --fedora              Download all Fedora versions,   will not download others unless selected
    --fedora-everything   Download Fedora Everything,     will not download others unless selected
    --fedora-kinoite      Download Fedora Kinoite,        will not download others unless selected
    --fedora-sericea      Download Fedora Sericea,        will not download others unless selected
    --fedora-server       Download Fedora Server,         will not download others unless selected
    --fedora-silverblue   Download Fedora Silverblue,     will not download others unless selected
    --fedora-spins        Download Fedora Spins,          will not download others unless selected
    --fedora-workstation  Download Fedora Workstation,    will not download others unless selected
    --kali                Download Kali,                  will not download others unless selected
    --opensuse            Download all opensuse versions, will not download others unless selected
    --opensuse-leap       Download opensuse,              will not download others unless selected
    --opensuse-tumbleweed Download opensuse tumbleweed,   will not download others unless selected
    --ubuntu              Download all Ubuntu versions,   will not download others unless selected
    --ubuntu-desktop      Download Ubuntu Desktop,        will not download others unless selected
    --ubuntu-server       Download Ubuntu Server,         will not download others unless selected
```

# Examples

```bash
Example:
    ./linux_downloader.sh -o ~/Downloads/ --iso --torrent --archlinux --ubuntu --fedora
    ./linux_downloader.sh -o ~/Downloads/ --ubuntu --fedora
    ./linux_downloader.sh -o ~/Downloads/ --iso --aria 5 --archlinux
```

# Supported File Types

*   Iso
*   Torrent

# Supported Distros

*   Archlinux
*   Debian
*   Fedora
    *   Everything
    *   Kinoite
    *   Sericea
    *   Server
    *   Silverblue
    *   Spins
    *   Workstation
*   Kali
*   OpenSUSE
    *   leap
    *   tumbleweed
*   Ubuntu
    *   Ubuntu Desktop
    *   Ubuntu Server
