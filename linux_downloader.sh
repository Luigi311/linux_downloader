#!/usr/bin/env bash

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

tester() {
    curl -ls "$testlink" > /dev/null 2>&1

    if [ "$?" -ne 0 ]; then
        die "ERROR: $testlink is invalid"
    fi
}

downloader() {
    echo "Downloading files"

    if [ "$aria" -eq 1 ]; then
        # Download $download_file with aria2
        aria2c --continue -i "${output}/${download_file}" -d "$output" -j "$parallel"
    else    
        # Download all files in $download_file with xargs and curl saving to output/basename
        (cd "$output" && xargs -n 1 -P "$parallel" curl -s --write-out "Downloaded %{filename_effective} in %{time_total}s\n"  -L -C - -O < "${download_file}")
    fi

    echo "Download complete"
}

validate_checksum() {
    echo "Validating checksums"

    # Check checksums if they exist
    if [ -f "${output}/${checksums256}" ]; then
        # validate files
        validation=$(cd "$output" && sha256sum -c "${checksums256}" --ignore-missing --quiet 2>&1)
    
        # Check for FAIILED
        failed=$(echo "$validation" | grep 'FAILED' | awk -F':' '{ print $1 }')

        if [ -n "$failed" ]; then
            echo "ERROR: $failed failed checksum validation"
            for i in $failed; do
                echo "Removing $i"
                rm -f "$output/$i"
            done
        fi
    fi

    echo "Checksum validation complete"
}

curl_link_helper() {
    curl -ls "$1" | grep -o '<a href=['"'"'"][^"'"'"']*['"'"'"]' | sed -e 's/^<a href=["'"'"']//' -e 's/["'"'"']$//'
}

get_link_folders() {
    folders=$(curl_link_helper "$1" | grep '/$')
    # strip ./ from the front of the folder
    folders=$(echo "$folders" | sed 's/^\.\///')
    # strip trailing slash
    folders=${folders%/}
    echo "$folders"
}

get_link_files() {
    files=$(curl_link_helper "$1" | grep -v '/$')
    # strip ./ from the front of the file
    files=$(echo "$files" | sed 's/^\.\///')
    echo "$files"
}

output="."

# Debugging information
test=0
type="Gathering"

# Download list
iso=0
torrent=0
all=1
archlinux=0
debian=0
fedora=0
fedora_everything=0
fedora_kinoite=0
fedora_sericea=0
fedora_server=0
fedora_silverblue=0
fedora_workstation=0
fedora_spins=0
kali=0
opensuse=0
opensuse_leap=0
opensuse_tumbleweed=0
ubuntu=0
ubuntu_desktop=0
ubuntu_server=0

# Optional Features
aria=0
parallel=1
download_only=0

# Source: http://mywiki.wooledge.org/BashFAQ/035
while :; do
    case "$1" in
        -h | -\? | --help)
            echo "Automatically download latest linux distributions"
            echo "Usage:"
            echo "    ./linux_downloader.sh [options]"
            echo ""
            echo "General Options:"
            echo "    -h, --help            Display this help message"
            echo "    -o, --output          Change the download directory, Default is current directory"
            echo "    --aria, --aria2       Download using aria2 instead of curl"
            echo "    --parallel            Amount of downloads in parallel via curl or aria2, default is 1"
            echo "    --iso                 Download iso files"
            echo "    --torrent             Download torrent files"
            echo "    --archlinux           Download Archlinux,             will not download others unless selected"
            echo "    --debian              Download Debian,                will not download others unless selected"
            echo "    --fedora              Download all Fedora versions,   will not download others unless selected"
            echo "    --fedora-everything   Download Fedora Everything,     will not download others unless selected"
            echo "    --fedora-kinoite      Download Fedora Kinoite,        will not download others unless selected"
            echo "    --fedora-sericea      Download Fedora Sericea,        will not download others unless selected"
            echo "    --fedora-server       Download Fedora Server,         will not download others unless selected"
            echo "    --fedora-silverblue   Download Fedora Silverblue,     will not download others unless selected"
            echo "    --fedora-spins        Download Fedora Spins,          will not download others unless selected"
            echo "    --fedora-workstation  Download Fedora Workstation,    will not download others unless selected"
            echo "    --kali                Download Kali,                  will not download others unless selected"
            echo "    --opensuse            Download all opensuse versions, will not download others unless selected"
            echo "    --opensuse-leap       Download opensuse,              will not download others unless selected"
            echo "    --opensuse-tumbleweed Download opensuse tumbleweed,   will not download others unless selected"
            echo "    --ubuntu              Download all Ubuntu versions,   will not download others unless selected"
            echo "    --ubuntu-desktop      Download Ubuntu Desktop,        will not download others unless selected"
            echo "    --ubuntu-server       Download Ubuntu Server,         will not download others unless selected"
            echo ""
            echo "Example:"
            echo "    ./linux_downloader.sh -o ~/Downloads/ --iso --torrent --archlinux --ubuntu --fedora"
            echo "    ./linux_downloader.sh -o ~/Downloads/ --ubuntu --fedora"
            echo "    ./linux_downloader.sh -o ~/Downloads/ --iso --aria 5 --archlinux"
            echo ""
            exit 0
            ;;
        -o | --output) # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                output="$2"
                # Strip trailing slash
                output=${output%/}
                mkdir -p "$output"
                shift
            else
                die 'ERROR: "--extension" requires a non-empty option argument.'
            fi
            ;;
        --test)
            test=1
            type="Testing"
            ;;
        --aria | --aria2)
            aria=1
            ;;
        --parallel)
            if [ "$2" ]; then
                parallel="$2"
                shift
            else
                die 'ERROR: "--parallel" requires a non-empty option argument.'
            fi
            ;;
        --iso)
            iso=1
            ;;
        --torrent)
            torrent=1
            ;;
        --archlinux)
            all=0
            archlinux=1
            ;;
        --debian)
            all=0
            debian=1
            ;;
        --fedora)
            all=0
            fedora=1
            fedora_everything=1
            fedora_kinoite=1
            fedora_sericea=1
            fedora_server=1
            fedora_spins=1
            fedora_silverblue=1
            fedora_workstation=1
            ;;
        --fedora-everything)
            all=0
            fedora=1
            fedora_everything=1
            ;;
        --fedora-kinoite)
            all=0
            fedora=1
            fedora_kinoite=1
            ;;
        --fedora-sericea)
            all=0
            fedora=1
            fedora_sericea=1
            ;;
        --fedora-server)
            all=0
            fedora=1
            fedora_server=1
            ;;
        --fedora-spins)
            all=0
            fedora=1
            fedora_spins=1
            ;;
        --fedora-silverblue)
            all=0
            fedora=1
            fedora_silverblue=1
            ;;
        --fedora-workstation)
            all=0
            fedora=1
            fedora_workstation=1
            ;;
        --kali)
            all=0
            kali=1
            ;;
        --opensuse)
            all=0
            opensuse=1
            opensuse_leap=1
            opensuse_tumbleweed=1
            ;;
        --opensuse-leap)
            all=0
            opensuse=1
            opensuse_leap=1
            ;;
        --opensuse-tumbleweed)
            all=0
            opensuse=1
            opensuse_tumbleweed=1
            ;;
        --ubuntu)
            all=0
            ubuntu=1
            ubuntu_desktop=1
            ubuntu_server=1
            ;;
        --ubuntu-desktop)
            all=0
            ubuntu=1
            ubuntu_desktop=1
            ;;
        --ubuntu-server)
            all=0
            ubuntu=1
            ubuntu_server=1
            ;;
        --download-only)
            all=0
            download_only=1
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

download_file="download_list.txt"
checksums256="checksums256.txt"

# Remove download list and checksum if it exists and not in download only mode
if [ $download_only -eq 0 ]; then
    rm -f "${output}/${download_file}"
    rm -f "${output}/${checksums256}"
fi

# Archlinux
if [[ $all -eq 1 || $archlinux -eq 1 ]]; then
    echo "$type Archlinux"

    arch_both=http://mirror.rackspace.com/archlinux/iso/latest
    checksumfile=sha256sums.txt
    
    if [ "$test" -eq 1 ]; then
        testlink="${arch_both}/${checksumfile}"
        tester "Archlinux"
    else
        # Get list of all files at a link with get_link_files
        file_list=$(get_link_files "${arch_both}/")

        for file in $file_list; do
            if [[ "$torrent" -eq 1 && $file == *".torrent" ]] ||
            [[ "$iso" -eq 1 && $file == *".iso" ]]; then
                echo "${arch_both}/${file}" >> "${output}/${download_file}"
            fi
        done

        # Download checksums and append to checksum file
        curl -ls "${arch_both}/${checksumfile}" >> "${output}/${checksums256}"
    fi

    echo "Archlinux Done"
fi

# Debian
if [[ $all -eq 1 || $debian -eq 1 ]]; then
    echo "$type Debian"

    debian_iso=ftp://cdimage.debian.org/debian-cd/current/amd64/iso-dvd
    debian_torrent=ftp://cdimage.debian.org/debian-cd/current/amd64/bt-dvd

    checksumfile=SHA256SUMS

    if [ "$test" -eq 1 ]; then
        testlink="${debian_torrent}/${checksumfile}"
        tester
    else
        if [ "$torrent" -eq 1 ]; then
            file_list=$(curl -ls "${debian_torrent}/")
            for file in $file_list; do
                if [[ $file == *".torrent" ]]; then
                    echo "${debian_torrent}/${file}" >> "${output}/${download_file}"
                fi
            done
        fi

        if [ "$iso" -eq 1 ]; then
            file_list=$(curl -ls "${debian_iso}/")
            for file in $file_list; do
                if [[ $file == *".iso" ]]; then
                    echo "${debian_iso}/${file}" >> "${output}/${download_file}"
                fi
            done

            # Download checksums and append to checksum file
            curl -s "${debian_iso}/${checksumfile}" >> "${output}/${checksums256}"
        fi
    fi
    echo "Debian Done"
fi

# Fedora
if [[ $all -eq 1 || $fedora -eq 1 ]]; then
    echo "$type Fedora"

    fedora_torrent=https://torrent.fedoraproject.org/torrents
    fedora_iso=https://mirror.arizona.edu/fedora/linux/releases

    if [ "$test" -eq 1 ]; then
        testlink="${fedora_torrent}/"
        tester
    else
        if [ "$torrent" -eq 1 ]; then
            file_list=$(get_link_files "${fedora_torrent}/" | grep -v "Beta")
            # Get the highest version number from the list of files by grabbing the the number right before the .torrent
            release=$(echo "$file_list" | awk -F'[-.]' '{print $(NF-1)}' | sort -n -r | awk NR==1)

            for file in $file_list; do
                # If the file contains the release number and .torrent then add it to the download list
                if [[ $file == *"$release"*".torrent" ]]; then
                    if [[ "$fedora_everything" -eq 1 && $file == *"Everything"* ]] ||
                    [[ "$fedora_kinoite" -eq 1 && $file == *"Kinoite"* ]] ||
                    [[ "$fedora_sericea" -eq 1 && $file == *"Sericea"* ]] ||
                    [[ "$fedora_server" -eq 1 && $file == *"Server"* ]] ||
                    [[ "$fedora_silverblue" -eq 1 && $file == *"Silverblue"* ]] ||
                    [[ "$fedora_spins" -eq 1 && $file == *"Spins"* ]] ||
                    [[ "$fedora_workstation" -eq 1 && $file == *"Workstation"* ]] ||
                    [[ "$all" -eq 1 ]]; then
                        echo "${fedora_torrent}/${file}" >> "${output}/${download_file}"
                    fi
                fi
            done
        fi

        if [ "$iso" -eq 1 ]; then
            # This curl requires -L so it can figure out the redirected link
            fedora=$(curl -Ls -o /dev/null -w %{url_effective} "$fedora_iso")
            fedora=${fedora%/}
            release=$(get_link_folders "${fedora}/" | sort -n -r | awk NR==1)
            release=${release%/}

            folder_list=$(get_link_folders "${fedora}/${release}/")

            for folder in $folder_list; do
                # strip trailing slash
                folder=${folder%/}
                if [[ "$fedora_everything" -eq 1 && "$folder" == "Everything" ]] ||
                [[ "$fedora_kinoite" -eq 1 && "$folder" == "Kinoite" ]] ||
                [[ "$fedora_sericea" -eq 1 && "$folder" == "Sericea" ]] ||
                [[ "$fedora_server" -eq 1 && "$folder" == "Server" ]] ||
                [[ "$fedora_silverblue" -eq 1 && "$folder" == "Silverblue" ]] ||
                [[ "$fedora_spins" -eq 1 && $folder == "Spins" ]] ||
                [[ "$fedora_workstation" -eq 1 && "$folder" == "Workstation" ]] ||
                [[ "$all" -eq 1 ]]; then
                    file_list=$(get_link_files "${fedora}/${release}/${folder}/x86_64/iso/")
                    for file in $file_list; do
                        if [[ $file == *".iso" ]]; then
                            echo "${fedora}/${release}/${folder}/x86_64/iso/${file}" >> "${output}/${download_file}"
                        
                        elif [[ $file == *"CHECKSUM" ]]; then
                            check=$(curl -s "${fedora}/${release}/${folder}/x86_64/iso/${file}")
                            # Extract SHA256 from check
                            echo "$check" | grep -i "SHA256 " | awk -F' ' '{print $4 "  " $2}' | sed 's/[()]//g' >> "${output}/${checksums256}"
                        fi
                    done
                fi
            done
        fi
    fi

    echo "Fedora Done"
fi

# Kali
if [[ $all -eq 1 || $kali -eq 1 ]]; then
    echo "$type Kali"

    kali_both=https://cdimage.kali.org/kali-images/current

    checksumfile=SHA256SUMS

    if [ "$test" -eq 1 ]; then
        testlink="${kali_both}/${checksumfile}"
        tester
    else
        file_list=$(get_link_files "${kali_both}/")
        for file in $file_list; do
            if [[ "$torrent" -eq 1 && $file == *".torrent" ]] || 
            [[ "$iso" -eq 1 && $file == *".iso" ]]; then
                echo "${kali_both}/${file}" >> "${output}/${download_file}"
            fi
        done

        # Download checksums and append to checksum file
        curl -L -s "${kali_both}/${checksumfile}" >> "${output}/${checksums256}"
    fi

    echo "Kali Done"
fi

# OpenSUSE
if [[ $all -eq 1 || $opensuse -eq 1 ]]; then
    echo "$type OpenSUSE"

    opensuse_leap_iso=https://download.opensuse.org/distribution/openSUSE-current/iso
    opensuse_tumbleweed_iso=https://download.opensuse.org/tumbleweed/iso

    if [ "$test" -eq 1 ]; then
        testlink="${opensuse_leap_iso}/"
        tester
    else
        if [ "$torrent" -eq 1 ]; then
            echo  "No official torrents available for opensuse"
        fi

        if [ "$iso" -eq 1 ]; then
            if [[ $opensuse_leap -eq 1 ]] ||
            [[ "$all" -eq 1 ]]; then
                file_list=$(get_link_files "${opensuse_leap_iso}/" | grep "Build")
                for file in $file_list; do
                    if [[ $file == *".iso" ]]; then
                        echo "${opensuse_leap_iso}/${file}" >> "${output}/${download_file}"
                    elif [[ $file == *".sha256" ]]; then
                        curl -s "${opensuse_leap_iso}/${file}" >> "${output}/${checksums256}"
                    fi
                done
            fi

            if [[ $opensuse_tumbleweed -eq 1 ]]||
            [[ "$all" -eq 1 ]]; then
                file_list=$(get_link_files "${opensuse_tumbleweed_iso}/" | grep "Snapshot")
                for file in $file_list; do
                    if [[ $file == *".iso" ]]; then
                        echo "${opensuse_tumbleweed_iso}/${file}" >> "${output}/${download_file}"
                    elif [[ $file == *".sha256" ]]; then
                        curl -s "${opensuse_tumbleweed_iso}/${file}" >> "${output}/${checksums256}"
                    fi
                done
            fi
        fi
    fi
    echo "OpenSUSE Done"
fi

# Ubuntu
if [[ $all -eq 1 || $ubuntu -eq 1 ]]; then
    echo "$type Ubuntu"
    
    ubuntu_ftp=ftp://releases.ubuntu.com/releases

    if [ "$test" -eq 1 ]; then
        testlink="${ubuntu_ftp}/"
        tester
    else
        release=$(curl -ls "${ubuntu_ftp}/" | grep -oE '[0-9]+(\.[0-9]+)*' | sort -n -r | awk NR==1)
        file_list=$(curl -ls "${ubuntu_ftp}/${release}/")

        for file in $file_list; do
            if [[ "$torrent" -eq 1 && $file == *".torrent" ]] ||
            [[ "$iso" -eq 1 && $file == *".iso" ]]; then
                if [[ "$ubuntu_server" -eq 1 && $file == *"server"* ]] ||
                [[ "$ubuntu_desktop" -eq 1 && $file == *"desktop"* ]] ||
                [[ "$all" -eq 1 ]]; then
                    echo "${ubuntu_ftp}/${release}/${file}" >> "${output}/${download_file}"
                fi
            fi
        done 
    fi

    echo "Ubuntu Done"
fi

if [ $test -eq 1 ]; then
    echo "Test Done"
    exit 0
else
    downloader
    validate_checksum
fi
